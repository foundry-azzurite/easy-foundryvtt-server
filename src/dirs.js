'use strict';

import {dirname, join} from 'path';
import {fileURLToPath} from 'url';

export default {
	get base() { return join(dirname(fileURLToPath(import.meta.url)), `..`)},
	get download() { return join(this.base, `_download`)},
	get userData() {
		if (!process.env.FOUNDRY_VTT_DATA_PATH) {
			throw new Error(`The environment variable FOUNDRY_VTT_DATA_PATH has to be set.`);
		}
		return process.env.FOUNDRY_VTT_DATA_PATH;
	},
	get data() { return join(this.userData, `Data`)},
	get systems() { return join(this.data, `systems`)},
	get modules() { return join(this.data, `modules`)},
	get app() { return join(this.base, `app`)},
	get appResources() {
		return join(this.app, `public`)
	}
}
