'use strict';

import commander from 'commander';
import {join} from 'path';
import {saveData} from './backup.js';
import {log, error, readLines, writeLines, logError} from './util.js';
import ENTITY_DATA from './entities/entity-data.js';
import dirs from './dirs.js';

const program = new commander.Command(`npm run maintenance`);

program.usage(`-- command`);

program
	.command(`mark-manually-installed <type> <name>`)
	.option(`-s, --save`, `immediately create a new backup commit in your backup repository`)
	.description(`Marks a system or module manually installed, adding it to your backup repository.`)
	.action(logError(async (type, name, {save}) => {
		const entityData = ENTITY_DATA[type];
		if (!entityData) {
			error(`The type must be one of [${Object.keys(ENTITY_DATA)}], given: ${type}`);
			process.exit(1);
		}
		const gitignore = join(dirs.userData, `.gitignore`);
		const ignores = await readLines(gitignore);
		const ignoreString = `!Data/${entityData.import}s/${name}/`;
		if (ignores.includes(ignoreString)) {
			log(`Already included in repo`);
		} else {
			ignores.push(ignoreString);
			await writeLines(gitignore, ignores);
			log(`Manual installation of ${type} ${name} added to backup repo`);
		}

		if (save) {
			await saveData(`Backup after manual installation of ${type}`);
		}
	}));

if (process.argv.length === 2) {
	program.help();
}

program.parseAsync(process.argv);
