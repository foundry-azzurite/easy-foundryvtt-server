'use strict';


import {logExecution, log, error, logError} from './util.js';
import {join} from 'path';
import dirs from './dirs.js';
import fs from 'fs-extra';
import {installSystems, installModules} from './entities/install-entity.js';
import {runCommand} from './util.js';

const GIT_IGNORE = `Config/options.json.lock
Logs/
Data/modules/*
!Data/modules/modules.json
Data/systems/*
!Data/systems/systems.json`;

// file types used by foundry in CONST.[IMAGE|VIDEO|AUDIO]_FILE_EXTENSIONS
const LFS_FILE_TYPES = [`jpg`, `jpeg`, `png`, `svg`, `webp`, `mp4`, `ogg`, `webm`, `flac`, `mp3`, `ogg`, `wav`, `webm`];

const GIT_ATTRIBUTES = LFS_FILE_TYPES
	.map((fileType) => `*.${fileType} filter=lfs diff=lfs merge=lfs -text`)
	.join(`\n`);

async function git(cmd) {
	return runCommand(`git ${cmd}`, {
		cwd: dirs.userData
	})
}

async function initRepo() {
	const gitignore = join(dirs.userData, `.gitignore`);
	if (await fs.exists(gitignore)) {
		log(`Already initialized, nothing to do.`);
		return;
	}
	await fs.writeFile(gitignore, GIT_IGNORE);
	await fs.writeFile(join(dirs.userData, `.gitattributes`), GIT_ATTRIBUTES);
	await git(`add -A`);
	await git(`commit -m "Initial commit"`);
	await git(`push --set-upstream origin master`);
}

logError(logExecution(async () => {
	await logExecution(initRepo)();
	await logExecution(installModules)();
	await logExecution(installSystems)();
}, `initData`))();
