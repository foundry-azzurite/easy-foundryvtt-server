import {join} from 'path';
import fs from 'fs-extra';
import {spawn} from 'child_process';
import readline from 'readline';
import {createReadStream} from 'fs';
import {createRequire} from 'module';
import dirs from './dirs.js';

const require = createRequire(import.meta.url);
let lockfile = null;

export function readdirSyncRecursiveImpl(dir, curdir = dir, fileList = []) {
	return fs.readdirSync(curdir, {withFileTypes: true}).flatMap(file => {
		const curFilePath = join(curdir, file.name);
		return file.isDirectory()
			? readdirSyncRecursiveImpl(dir, curFilePath, fileList)
			: fileList.concat(relative(dir, curFilePath));
	});
}

async function spawnWithDefaults(cmd, args, options = {}) {
	options.env = options.env || {};
	options.env = {
		ADBLOCK: `true`,
		...process.env,
		...options.env
	};
	let finalOptions = {
		encoding: `utf8`,
		shell: true,
		...options
	};
	const execution = spawn(cmd, args, finalOptions);

	return new Promise((resolve, reject) => {
		let stdout = ``;

		execution.stdout.on(`data`, (data) => {
			if (!finalOptions.quiet) {
				process.stdout.write(data);
			}
			stdout += data.toString(`utf8`);
		});

		let stderr = ``;
		execution.stderr.on(`data`, (data) => {
			if (!finalOptions.quiet) {
				process.stdout.write(data);
			}
			stderr += data.toString(`utf8`);
		});

		execution.on('error', () => {
			reject({
				error
			});
		});

		execution.on('close', (code) => {
			const result = {
				stdout,
				stderr,
				code
			};
			resolve(result);
		});
	});
}

export async function runCommand(cmd, options = {}) {
	const name = options.name ? `${options.name}: ` : '';
	const runlog = !options.quiet && !options.noRunLog;
	if (runlog) {
		log(`Running command ${name}"${cmd}"`);
	}
	const [actualCmd, ...args] = cmd.split(` `);
	const out = await spawnWithDefaults(actualCmd, args, options);
	if (runlog) {
		log(`Finished running command ${name}"${cmd}"`);
	}
	debug(`command "`,actualCmd, ...args, `" returned `, out);
	return out;
}

export function mockGlobalExpressEmit() {
	global.express = {io: {emit: () => {}}};
}

export async function readLines(path) {
	const reader = readline.createInterface({
		input: createReadStream(path)
	});

	const lines = [];
	for await (const line of reader) {
		lines.push(line);
	}
	return lines;
}

export async function writeLines(path, lines) {
	return fs.writeFile(path, lines.join(`\n`));
}

let logNesting = 0;

function logIndent() {
	return `  `.repeat(logNesting);
}

export function log(...args) {
	console.log(logIndent(), ...args);
}

export function error(...args) {
	console.error(logIndent(), ...args);
}

export function debug(...args) {
	if (`DEBUG` in process.env) {
		console.debug(logIndent(), new Date(), `DEBUG -`, ...args);
	}
}

function logWithArgs(msg, args) {
	const msgWithArgs = args.length ? `${msg} with arguments` : msg;
	log(msgWithArgs, ...args);
}

export function logExecution(fn, name = fn.name || `anonymous function`) {
	return async (...args) => {
		logWithArgs(`Starting ${name}`, args);
		logNesting++;
		const result = await fn(...args);
		logNesting--;
		logWithArgs(`Finished ${name}`, args);
		return result;
	};
}

export async function isAppRunning() {
	if (!lockfile) lockfile = require(join(dirs.app, `node_modules`, `proper-lockfile`));
	return await lockfile.check(join(dirs.userData, `Config`, `options.json`));
}

export function logError(fn) {
	return async (...args) => {
		try {
			return await fn(...args);
		} catch (e) {
			if (e) {
				error(e && e.message);
			}
			if (process.env.DEBUG) throw e;
			process.exit(1);
		}
	};
}

export function logErrorSync(fn) {
	return (...args) => {
		try {
			return fn(...args);
		} catch (e) {
			if (e) {
				error(e && e.message);
			}
			if (process.env.DEBUG) throw e;
			process.exit(1);
		}
	};
}
