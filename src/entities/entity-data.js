'use strict';

import dirs from '../dirs.js';
import {join} from 'path';

const ENTITY_DATA = {
	module: {
		import: `module`,
		className: `Module`,
		get dataDir() { return dirs.modules; },
		manifestFile: `module.json`,
		get listFile() { return join(dirs.modules, `modules.json`); }
	},
	system: {
		import: `system`,
		className: `System`,
		get dataDir() { return dirs.systems; },
		manifestFile: `system.json`,
		get listFile() { return join(dirs.systems, `systems.json`); }
	}
};

export default ENTITY_DATA;
