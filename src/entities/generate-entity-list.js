'use strict';

import ENTITY_DATA from './entity-data.js';
import fs from 'fs-extra';
import path from 'path';
import {error} from '../util.js';


export default function generateEntityList(entity) {
	const data = ENTITY_DATA[entity];
	const files = fs.readdirSync(data.dataDir, {withFileTypes: true});
	const manifestUrls = files
		.filter(f => f.isDirectory())
		.map(dir => path.join(data.dataDir, dir.name, data.manifestFile))
		.map(manifestFile => fs.readJsonSync(manifestFile))
		.map(manifest => {
			if (!manifest.manifest) {
				error(`Module ${manifest.name} does not contain URL to its manifest, can not save!`);
				return null;
			}
			return manifest.manifest;
		})
		.filter(manifestUrl => manifestUrl !== null)
		.sort((str1, str2) => str1.toLowerCase().localeCompare(str2.toLowerCase()));
	fs.writeJsonSync(data.listFile, manifestUrls, {spaces: `\t`});
}
