'use strict';

import {createRequire} from 'module';
import fs from 'fs-extra';
import {join} from 'path';
import {mockGlobalExpressEmit, log, error} from '../util.js';
import dirs from '../dirs.js';
import ENTITY_DATA from './entity-data.js';

const require = createRequire(import.meta.url);

global.publicDir = dirs.appResources;
mockGlobalExpressEmit();


function readExistingManifest(data, name) {
	const manifestPath = join(data.dirs.data, name, `${data.import}.json`);
	if (!fs.existsSync(manifestPath)) {
		return null;
	}

	return fs.readJsonSync(manifestPath);
}


export default async function installEntity(entity, force = false) {
	const data = ENTITY_DATA[entity];

	global.paths = {data: dirs.data};
	global.logger = {
		info(...args) {},
		error(...args) { error(...args) }
	};

	const {[data.className]: Entity} = require(join(dirs.app, `dist`, `packages`, data.import));
	const {isNewerVersion} = require(join(dirs.app, `dist`, `common`, `utils`));

	if (force) {
		fs.readdirSync(data.dirs.data, {withFileTypes: true})
			.filter(f => f.isDirectory())
			.forEach(f => fs.removeSync(join(data.dirs.data, f.name)));
	}

	if (!fs.existsSync(data.listFile)) {
		log(`No ${data.import}s to install`);
		return;
	}

	const installFromManifest = async (manifestPromise) => {
		const manifest = await manifestPromise;

		const existingManifest = readExistingManifest(data, manifest.name);
		if (existingManifest && !isNewerVersion(manifest.version, existingManifest.version)) {
			log(`${manifest.name} already at latest version`);
			return;
		}
		log(`Installing ${manifest.name}`);
		try {
			await Entity.install(manifest.name, manifest.download);
			log(`Finished installing ${manifest.name}`);
		} catch (e) {
			error(`Error while installing ${manifest.name}`, e);
		}
	};

	const manifestPromises = JSON.parse(fs.readFileSync(data.listFile))
		.map(Entity.getManifest);
	for (const manifestPromise of manifestPromises) {
		await installFromManifest(manifestPromise);
	}
};

export async function installModules(force = false) {
	return await installEntity(`module`, force);
}
export async function installSystems(force = false) {
	return await installEntity(`system`, force);
}
