'use strict';
import path from 'path';
import fs from 'fs-extra';
import download from 'download';

import dirs from './dirs.js';
import {error, logError, logExecution} from './util.js';

const zipUrl = process.argv[2];

const downloadFoundry = logExecution(async () => {
	await download(zipUrl, dirs.download, {
		extract: true,
		filter: file => {
			return file.path.startsWith('resources/app/');
		}
	});
}, `downloadFoundry`);

logError(logExecution(async () => {
	await fs.removeSync(dirs.download);

	if (fs.existsSync(dirs.app)) {
		error(`Can't install, app/ already exists. Use update.`);
		process.exit(1);
	}

	await downloadFoundry();

	var unzippedAppDir = path.join(dirs.download, `resources`, `app`);
	await fs.moveSync(unzippedAppDir, dirs.app);

	await fs.removeSync(dirs.download);
}, `installFoundry`))();
