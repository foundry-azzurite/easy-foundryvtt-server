'use strict';

import {error, logError, logExecution} from './util.js';
import {saveData} from './backup.js';



const backupMessageTitle = process.argv[2] || 'Manual backup';
logError(logExecution(async () => {
	try {
		await saveData(backupMessageTitle);
	} catch (e) {
		error(e.message);
		process.exit(1);
	}
}, `save`))();
