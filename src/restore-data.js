'use strict';

import {restoreData} from './backup.js';
import {error} from './util.js';


const commitToRestore = process.argv[2];
logErrorSync(restoreData)(commitToRestore);
