'use strict';


import {error, log, logExecution, runCommand, isAppRunning} from './util.js';
import dirs from './dirs.js';
import generateEntityList from './entities/generate-entity-list.js';
import {installModules, installSystems} from './entities/install-entity.js';

async function git(args, options = {}) {
	const result = await runCommand(`git ${args}`, {
		quiet: true,
		cwd: dirs.data,
		...options
	});
	if (options.quiet && result.exitCode !== 0) {
		error(`Error while executing git command git ${args}`);
		if (result.stderr.trim() !== ``) {
			error(`Error message from git: ${result.stderr}`);
		}
		error(`This is likely not a problem with the server software but with something related to your environment.`);
	}
	return result;
}

async function hasRemoteUpdate() {
	const headHash = await git(`rev-parse HEAD`).stdout;
	const masterHash = await git(`rev-parse origin/master`).stdout;
	return headHash !== masterHash;
}

async function hasChanges() {
	return !await noChanges();
}

async function noChanges() {
	let output = (await git(`status --porcelain`)).stdout;
	return output === ``;
}


async function push() {
	await logExecution(() => git(`push`, {
		quiet: false,
		noRunLog: true
	}), `pushing changes to backup repository`)();
}

async function commit(title, explanation) {
	await git(`commit -m "${title}" -m "${explanation}"`);
}

async function publishChanges(title, explanation) {
	if (!title) throw new Error(`Commit needs title`);
	if (!explanation) {
		explanation =
			`This is an automatically created commit bringing the newest changes from the game to the repository`;
	}
	log(`Committing changes to backup repository`);
	await git(`add -A`);
	await commit(title, explanation);
	await push();
}

export async function saveData(commitTitle, commitExplanation) {
	generateEntityList(`module`);
	generateEntityList(`system`);

	if (await noChanges()) {
		log(`No changes, not backing anything up.`);
		return;
	}

	await git(`fetch`);

	if (await hasRemoteUpdate()) {
		throw new Error(`Data repository is behind remote! This shouldn't happen, as we try to update` +
			` remote, not the other way around. Fix this manually.`);
	}

	await publishChanges(commitTitle, commitExplanation);
}

async function commitMissing(commitId) {
	return (await git(`cat-file -e "${commitId}^{commit}"`)).code !== 0;
}

async function hasNoChangesToHead(commitId) {
	return !(await git(`diff ${commitId}..HEAD`)).stdout;
}

async function fullHash(commitId) {
	return (await git(`rev-parse ${commitId}`)).stdout.trim();
}

async function shortHash(commitId) {
	return (await git(`rev-parse --short ${commitId}`)).stdout.trim();
}

export async function restoreData(commitId) {
	if (await isAppRunning()) {
		error(`FoundryVTT server is running. Please shut down the server before trying to restore a commit.`);
		return;
	}
	if (!commitId) {
		error(`No commit given`);
		return;
	}

	if (await commitMissing(commitId)) {
		error(`The given commit could not be found, are you sure ${commitId} is a valid commit?`);
		return;
	}
	const hash = await fullHash(commitId);
	const hsh = await shortHash(commitId);

	log(`Backing up current state before restore.`);
	await saveData(`Backup before restore`, `This is an automatically created commit backing up changes ` +
		`in the data directory that were still pending when the restore of a previous commit was requested.`);

	if (await hasNoChangesToHead(hash)) {
		log(`The data in the given commit is the same as it is now, so there's nothing to restore.`);
		return;
	}

	await git(`revert --no-commit ${hash}..HEAD`);
	await commit(`Restore commit ${hsh}`, `This is an automatically generated commit that reverts the data` +
		` directory to the contents it had in commit ${hash}.`);
	await logExecution(installModules)(true);
	await logExecution(installSystems)(true);
	await push();
	log(`Commit ${hsh} has been successfully restored.`)
}
