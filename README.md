This repository shows you how to set up a foundry server on an ubuntu machine so that:

1. You have daily backups of your data directory in a git repository
1. You can automatically install/update/backup/restore everything whenever you want, with simple commands
1. Foundry runs as a service and on boot

## Requirements

* An Ubuntu machine (Other distros maybe work as well, check [`install.sh`](install/install.sh) yourself)
* Git installed
* Node (v10+) installed
* At least basic knowledge about how the above technologies work, this is not made for a complete layman

## How to use this for yourself

### Install

1. Create a new empty git repository somewhere, this will be your backup repository
   * Note the clone URL, for this repository for example `git@gitlab.com:foundry-azzurite/easy-foundryvtt-server.git`
   * Make sure wherever you want to install has commit access to this repo
1. Download the [`install.sh`](https://gitlab.com/foundry-azzurite/server/-/raw/master/install/install.sh) to your
 machine
1. Run `install.sh` with root permissions (before you do this, you may want to check this file if you don't trust me)
   * The first parameter has to be the FoundryVTT download link
   * The second parameter has to be a git url to clone your backup repository
   * The environment variable `FOUNDRY_VTT_DATA_PATH` has to be set to a data path you want (must not exist yet)
   * Example: `FOUNDRY_VTT_DATA_PATH=/opt/foundry-data sudo -E ./install.sh https://foundry/zip git@gitlab.com
   :azzurite/foundry-backup-test.git`
1. (Optional) Copy your existing data to whatever you set `FOUNDRY_VTT_DATA_PATH` to
1. (Optional) Adjust the line `ExecStart=/usr/bin/npm start` in `/etc/systemd/system/foundry.service` to your liking.
   
   For example, you could add additional parameters like `--world` or `--port`:
   
   ```
   ExecStart=/usr/bin/npm start -- --world=test --port=30001
   ```
   
### Usage

After installation, your FoundryVTT server should be running. Use `service foundry status` to show its status. It's a normal system.d service so everything related should work for it.

If you want to restore any previous version of your data, find the commit in your backup repository and do `npm run
 restore-data -- <commit id>`.

If I update this repo in the future I will provide upgrade steps.

Here is a list of the scripts to provide the above features. They can be called with `npm run <script> -- <args>` and are listed here:

| Script | Arguments | Description |
| ------ | ---- | ----------- |
| start  | Any that foundry itself takes | Starts the foundry server. |
| install-foundry | The FoundryVTT download link | Installs FoundryVTT in the `/opt/foundry-server/app/` directory |
| save-data | None | Saves the data in your data directory |
| restore-data | A git SHA1 commit hash | Restores your data to the state of the given commit |
| maintenance | mark-manually-installed <system/module> \<name> | Currently allows you to register a module that doesn't have a download URL in its manifest to be added to the backup repository. Will later provide various miscellaneous actions to manage the repo |

## How it works

There's a system.d service file that basically runs `npm start`.

There's a cron job that runs `npm run save-data` daily.

There's a setup script that:
 
* Clones your backup repository to the data directory you provided
* Clones this repository
* Runs the `install-foundry` script
* Creates a user (`foundry`) the server will run on
* Installs the system.d service and sets it to run on boot
* Adds the save-data cron job to `/etc/cron.daily`

### Request from me to you

Please report any problems you have reading this README or just simple hard to understand parts. I want to improve the documentation so it's as easy as possible to set everything up.
