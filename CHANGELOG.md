# 2.0.0

* Remove update-foundry script. It is now encouraged to update foundry through the normal foundry web interface.
* Add backup git error reporting

# 1.0.1

* Fixes the save-data command not working

# 1.0.0

Initial release
