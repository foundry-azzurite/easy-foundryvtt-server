#!/usr/bin/env bash
set -o errexit


if [[ -z "$FOUNDRY_VTT_DATA_PATH" ]]; then
  >&2 echo "You have to set the FOUNDRY_VTT_DATA_PATH environment variable to an empty directory of your choice."
  exit 1
fi

if [[ -d "$FOUNDRY_VTT_DATA_PATH" ]]; then
  >&2 echo "The dir in the env variable FOUNDRY_VTT_DATA_PATH ($FOUNDRY_VTT_DATA_PATH) already exists. This
  install script can only handle fresh installations."
  >&2 echo "If you already have existing data, empty that directory, perform the installation, and then copy your
  existing data to the new directory."
  exit 1
fi

if [[ -z "$1" ]]; then
    >&2 echo "You have to supply the FoundryVTT download url as an argument to this script."
    exit 1
fi
foundry_download_url="$1"

if [[ -z "$2" ]]; then
    >&2 echo "You have to supply the backup repository url as an argument to this script."
    exit 1
fi
backup_repo="$2"

useradd -r -s /bin/false foundry
usermod -L foundry

apt-get install git-lfs
git lfs install


git clone "$backup_repo" "$FOUNDRY_VTT_DATA_PATH"
chown -R foundry:foundry "$FOUNDRY_VTT_DATA_PATH"

mkdir -p /opt/foundry-server
cd /opt/foundry-server || exit 1
git clone git@gitlab.com:foundry-azzurite/easy-foundryvtt-server.git .
npm ci
npm run install-foundry -- "$foundry_download_url"
npm run init-data
chown -R foundry:foundry .

FOUNDRY_SERVICE_FILE=/etc/systemd/system/foundry.service
if [[ ! -f $FOUNDRY_SERVICE_FILE ]]; then
echo "
[Unit]
Description=FoundryVTT Server
After=network.target
Requires=network.target

StartLimitIntervalSec=5
StartLimitBurst=0

[Service]
User=foundry
Group=foundry
WorkingDirectory=/opt/foundry-server
ExecStart=/usr/bin/npm start
Restart=always
Environment=FOUNDRY_VTT_DATA_PATH=$FOUNDRY_VTT_DATA_PATH

[Install]
WantedBy=multi-user.target
" > $FOUNDRY_SERVICE_FILE
fi
systemctl daemon-reload
service foundry start
systemctl enable foundry

FOUNDRY_CRON_FILE=/etc/cron.daily/foundry-save.sh
if [[ ! -f $FOUNDRY_CRON_FILE ]]; then
echo "
#!/usr/bin/env bash

cd /opt/foundry-server || exit 1
FOUNDRY_VTT_DATA_PATH=$FOUNDRY_VTT_DATA_PATH npm run save-data -- "Automatic Backup"
" > $FOUNDRY_CRON_FILE
chmod +x $FOUNDRY_CRON_FILE
fi
